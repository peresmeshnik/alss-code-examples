<?php

namespace AppBundle\BusinessEntity\Size\BusinessEntity;

class Size
{
    const MICRO = 1;
    const SMALL = 2;
    const MIDDLE = 3;
    const LARGE = 4;
}