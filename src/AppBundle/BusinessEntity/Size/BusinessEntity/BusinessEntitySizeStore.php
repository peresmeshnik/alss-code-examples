<?php

namespace AppBundle\BusinessEntity\Size\BusinessEntity;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\RecentBusinessEntityStore;
use AppBundle\BusinessEntity\Size\BusinessEntity\Exception\UnexpectedValueException;
use AppBundle\BusinessEntity\Size\SmallBusiness\SmallBusinessStore;
use AppBundle\BusinessEntity\Size\SmallBusiness\UpdateMetadataStore;

class BusinessEntitySizeStore
{
    /**
     * @var SmallBusinessStore
     */
    private $smallBusinessesStore;

    /**
     * @var RecentBusinessEntityStore
     */
    private $recentBusinessEntityStore;


    public function __construct(
        SmallBusinessStore $smallMiddleBusinessEntityStore,
        RecentBusinessEntityStore $recentBusinessEntityStore
    ) {
        $this->smallBusinessesStore = $smallMiddleBusinessEntityStore;
        $this->recentBusinessEntityStore = $recentBusinessEntityStore;
    }

    /**
     * @return int|null
     */
    public function getFor(array $businessEntity)
    {
        $smallBusiness = $this->smallBusinessesStore->getOneByInn($businessEntity['inn'] ?? null);

        if ($smallBusiness) {
            return $smallBusiness->category;
        } elseif ($businessEntity['isDissolved']) {
            return null;
        } elseif (!isset($businessEntity['regDate'])) {
            return null;
        } elseif ($this->recentBusinessEntityStore->contains($businessEntity)) {
            return null;
        } else {
            return Size::LARGE;
        }
    }
}