<?php

namespace AppBundle\BusinessEntity\Size\BusinessEntity;

use AppBundle\BusinessEntity\Size\BusinessEntity\BusinessEntitySizeStore;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\BusinessEntityStore;
use AppBundle\Core\MongoDB\BatchCollectionWalker;

class UpdateSizesCommand
{
    /**
     * @var BatchCollectionWalker
     */
    private $businessEntitiesWalker;

    /**
     * @var BusinessEntityStore
     */
    private $businessEntityStore;

    /**
     * @var BusinessEntitySizeStore
     */
    private $businessEntiySizeStore;


    public function __construct(
        BatchCollectionWalker $businessEntitiesWalker,
        BusinessEntityStore $businessEntityStore,
        BusinessEntitySizeStore $businessEntiySizeStore
    ) {
        $this->businessEntitiesWalker = $businessEntitiesWalker;
        $this->businessEntityStore = $businessEntityStore;
        $this->businessEntiySizeStore = $businessEntiySizeStore;
    }

    public function run()
    {
        $this->businessEntitiesWalker->setFilter(['isDissolved' => false]);
        $this->businessEntitiesWalker->setBatchHandler(function ($businessEntities) {
            foreach ($businessEntities as $businessEntity) {
                $this->businessEntityStore->updateSize(
                    $businessEntity['ogrn'],
                    $this->businessEntiySizeStore->getFor($businessEntity)
                );
            }
        });
        $this->businessEntitiesWalker->walk();
    }
}