<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness;

class Type
{
    const COMPANY = 1;
    const ENTREPRENEUR = 2;
}