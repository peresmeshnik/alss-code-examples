<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateMetadataStore as CommonUpdateMetadataStore;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;
use AppBundle\Core\Store\ClearableStore;
use AppBundle\Document\UpdateMetadataType;
use MongoDB\Collection;

class UpdateMetadataStore implements ClearableStore, CommonUpdateMetadataStore
{
    /**
     * @var Collection
     */
    private $collection;


    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function replaceWith(UpdateMetadata $metadata)
    {
        $this->clear();
        $this->collection->insertOne($metadata);
    }

    /**
     * @return UpdateMetadata|null
     */
    public function get()
    {
        /** @var UpdateMetadata|null $result */
        $result = $this->collection->findOne(
            [ 'type' => UpdateMetadataType::SMALL_MIDDLE_ENTITY_UPDATE ],
            [
                'typeMap' =>[
                    'root' => UpdateMetadata::class
                ]
            ]
        );

        return $result;
    }

    public function clear()
    {
        $this->collection->deleteMany(['type' => UpdateMetadataType::SMALL_MIDDLE_ENTITY_UPDATE]);
    }
}