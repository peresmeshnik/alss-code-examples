<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdatePathToUpdateMetadataConverter as CommonUpdatePathToUpdateMetadataConverter;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;

class UpdatePathToUpdateMetadataConverter extends CommonUpdatePathToUpdateMetadataConverter
{
    const DEFAULT_DATA_DATE_FORMAT = 'mdY';

    protected function createUpdateMetadataFrom(string $updateFilename, \Datetime $dataDate)
    {
        return UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            $updateFilename,
            $dataDate->format('Y-m-d')
        );
    }
}