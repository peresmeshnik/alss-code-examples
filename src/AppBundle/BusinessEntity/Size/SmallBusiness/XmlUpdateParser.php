<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\XmlUpdateParser as CommonXmlUpdateParser;
use AppBundle\BusinessEntity\Size\SmallBusiness\Exception\RuntimeException;
use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use AppBundle\CompanyInfo\PathableDocument;
use AppBundle\EgrulEgripUpdates\StandardXmlUpdateToPathablesConverter;

class XmlUpdateParser implements CommonXmlUpdateParser
{
    const ID_PATH = '@attributes.ИдДок';
    const COMPANY_INN_PATH = 'ОргВклМСП.@attributes.ИННЮЛ';
    const ENTREPRENEUR_INN_PATH = 'ИПВклМСП.@attributes.ИННФЛ';
    const CATEGORY_PATH = '@attributes.КатСубМСП';
    const TYPE_PATH = '@attributes.ВидСубМСП';

    /**
     * @var StandardXmlUpdateToPathablesConverter
     */
    private $xmlToPathableConverter;


    public function __construct()
    {
        $this->xmlToPathableConverter = new StandardXmlUpdateToPathablesConverter();
    }

    /**
     * @return SmallBusiness[]
     */
    public function parse(string $xmlUpdate): array
    {
        $result = [];
        $xmlUpdateEntries = $this->xmlToPathableConverter->convertString($xmlUpdate);

        foreach ($xmlUpdateEntries as $xmlUpdateEntry) {
            if ($this->validate($xmlUpdateEntry)) {
                $result[] = $this->parseXmlUpdateEntry($xmlUpdateEntry);
            }
        }

        return $result;
    }

    private function validate(PathableDocument $xmlUpdateEntry): bool
    {
        return $xmlUpdateEntry->doesPathExist(self::ID_PATH);
    }

    private function parseXmlUpdateEntry(PathableDocument $updateEntry): SmallBusiness
    {
        $smallMiddleEntity = $this->makeSmallMiddleEntityWithOnlyCategorySetFrom($updateEntry);

        if ($this->isCompany($updateEntry)) {
            return $smallMiddleEntity->company()
                ->withInn(
                    $updateEntry->getValueByPathOrThrow(self::COMPANY_INN_PATH)
                )
            ;
        } elseif ($this->isEntrepreneur($updateEntry)) {
            return $smallMiddleEntity->entrepreneur()
                ->withInn(
                    $updateEntry->getValueByPathOrThrow(self::ENTREPRENEUR_INN_PATH)
                )
            ;
        } else {
            throw new RuntimeException(
                sprintf(
                "Unexpected small middle entity type:\n%s",
                    var_export($smallMiddleEntity, true)
                )
            );
        }
    }

    private function makeSmallMiddleEntityWithOnlyCategorySetFrom(PathableDocument $smallMiddleEntity): SmallBusiness
    {
        $category = $smallMiddleEntity->getValueByPathOrNull(self::CATEGORY_PATH);

        switch ($category) {
            case Category::MICRO:
                return SmallBusiness::aMicro();
            case Category::SMALL:
                return SmallBusiness::aSmall();
            case Category::MIDDLE;
                return SmallBusiness::aMiddle();
                break;
            default:
                throw new RuntimeException(
                    sprintf(
                        'Unexpected small middle entity category: %s.',
                        var_export($category, true)
                    )
                );
        }
    }

    private function isCompany(PathableDocument $smallMiddleEntity): bool
    {
        return $smallMiddleEntity->getValueByPathOrNull(self::TYPE_PATH) == Type::COMPANY;
    }

    private function isEntrepreneur(PathableDocument $smallMiddleEntity): bool
    {
        return $smallMiddleEntity->getValueByPathOrNull(self::TYPE_PATH) == Type::ENTREPRENEUR;
    }
}