<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Store;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateEntryStore;
use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use MongoDB\Collection;

class SmallBusinessStore implements UpdateEntryStore
{
    /**
     * @var Collection
     */
    private $collection;


    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param SmallBusiness $specialTaxation
     */
    public function add($specialTaxation)
    {
        $this->collection->insertOne($specialTaxation);
    }

    /**
     * @param string $inn
     * @return SmallBusiness|null
     */
    public function getOneByInn($inn)
    {
        /** @var SmallBusiness $result */
        $result = $this->collection->findOne(
            [ 'inn' => $inn ],
            [
                'typeMap' =>[
                    'root' => SmallBusiness::class
                ]
            ]
        );

        return $result;
    }

    public function clear()
    {
        $this->collection->deleteMany([]);
    }
}