<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness;

class Category
{
    const MICRO = 1;
    const SMALL = 2;
    const MIDDLE = 3;
}