<?php

namespace AppBundle\BusinessEntity\Size\SmallBusiness\Model;

use AppBundle\BusinessEntity\Size\SmallBusiness\Category;
use AppBundle\BusinessEntity\Size\SmallBusiness\Type;
use MongoDB\BSON\Persistable;

class SmallBusiness implements Persistable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    public $inn;

    /**
     * @var string
     */
    public $type;

    /**
     * @var int
     */
    public $category;


    public static function aMicro(): self
    {
        $result = new self;
        $result->category = Category::MICRO;

        return $result;
    }

    public static function aSmall(): self
    {
        $result = new self;
        $result->category = Category::SMALL;

        return $result;
    }

    public static function aMiddle(): self
    {
        $result = new self;
        $result->category = Category::MIDDLE;

        return $result;
    }

    public function company(): self
    {
        $this->type = Type::COMPANY;

        return $this;
    }

    public function entrepreneur(): self
    {
        $this->type = Type::ENTREPRENEUR;

        return $this;
    }

    public function withInn(string $inn): self
    {
        $this->inn = $inn;

        return $this;
    }

    function bsonSerialize()
    {
        return [
            'inn' => $this->inn,
            'type' => $this->type,
            'category' => $this->category,
        ];
    }

    function bsonUnserialize(array $data)
    {
        $this->inn = $data['inn'];
        $this->type = $data['type'];
        $this->category = $data['category'];
    }
}