<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;

class ParseZipUpdateCommand
{
    /**
     * @var ZipUpdateParser
     */
    private $zipUpdateParser;

    /**
     * @var UpdateMetadataStore
     */
    private $updateMetadataStore;

    /**
     * @var UpdatePathToUpdateMetadataConverter
     */
    private $updatePathToMetadataConverter;


    public function __construct(
        ZipUpdateParser $zipUpdateParser,
        UpdateMetadataStore $updateMetadataStore,
        UpdatePathToUpdateMetadataConverter $updatePathToMetadataConverter
    ) {
        $this->zipUpdateParser = $zipUpdateParser;
        $this->updateMetadataStore = $updateMetadataStore;
        $this->updatePathToMetadataConverter = $updatePathToMetadataConverter;
    }
    public function run(string $updatePath)
    {
        $this->zipUpdateParser->parse($updatePath);
        $this->updateMetadataStore->replaceWith($this->makeUpdateMetadataFrom($updatePath));
    }

    private function makeUpdateMetadataFrom(string $updatePath): UpdateMetadata
    {
        return $this->updatePathToMetadataConverter->convert($updatePath);
    }
}