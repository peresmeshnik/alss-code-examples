<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Size\BusinessEntity\Exception\UnexpectedValueException;

class RecentBusinessEntityStore
{
    const DEFAULT_BEFORE_DAYS = 30;

    /**
     * @var UpdateMetadataStore
     */
    private $updateMetadataStore;

    /**
     * @var int
     */
    private $beforeDays;


    public function __construct(UpdateMetadataStore $updateMetadataStore, int $beforeDays = self::DEFAULT_BEFORE_DAYS)
    {
        $this->updateMetadataStore = $updateMetadataStore;
        $this->beforeDays = $beforeDays;
    }

    public function contains(array $businessEntity): bool
    {
        $updateMetadata = $this->updateMetadataStore->get();

        if (!$updateMetadata) {
            throw new UnexpectedValueException('No update metadata.');
        }

        return $this->getDifferenceInDaysBetween($updateMetadata->dataDate, $businessEntity['regDate'])
            < self::DEFAULT_BEFORE_DAYS;
    }

    private function getDifferenceInDaysBetween(string $first, string $second)
    {
        $firstDate = new \DateTime($first);
        $secondDate = new \DateTime($second);

        return $firstDate->diff($secondDate)->format('%a');
    }
}