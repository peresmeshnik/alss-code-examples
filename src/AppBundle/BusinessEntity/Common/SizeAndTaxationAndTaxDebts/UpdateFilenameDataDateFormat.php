<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

class UpdateFilenameDataDateFormat
{
    const SPECIAL_TAXATION_REGIMES = 'Ymd';
    const SMALL_BUSINESSES = 'mdY';
}