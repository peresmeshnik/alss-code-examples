<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

interface XmlUpdateParser
{
    public function parse(string $xmlUpdate): array;
}