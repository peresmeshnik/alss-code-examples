<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use Psr\Log\LoggerInterface;

class ZipUpdateParserReporter
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function reportClearingStart()
    {
        $this->logger->notice('Removing existing data.');
    }
}