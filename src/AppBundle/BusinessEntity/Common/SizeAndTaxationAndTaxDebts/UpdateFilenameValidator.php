<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\Common\Util\RegExp;

class UpdateFilenameValidator
{
    const UPDATE_FILENAME_REGEXP = '/^data-\d{8}-structure-%s.zip$/';
    const DEFAULT_STRUCTURE_VERSION_REGEXP = '\d{8}';

    /**
     * @var string
     */
    private $structureVersionRegExp;


    public function __construct(string $structureVersion)
    {
        $this->structureVersionRegExp = self::DEFAULT_STRUCTURE_VERSION_REGEXP;
        $this->structureVersionRegExp = $structureVersion;
    }

    public function validate(string $updateFilename): bool
    {
        return (new RegExp($this->getUpdateFilenameRegExp()))->test($updateFilename);
    }

    private function getUpdateFilenameRegExp(): string
    {
        return sprintf(self::UPDATE_FILENAME_REGEXP, $this->structureVersionRegExp);
    }
}