<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use MongoDB\Collection;

class BusinessEntityStore
{
    const SIZE_FIELD_NAME = 'size';
    const TAXATION_FIELD_NAME = 'taxationRegimes';
    const NUMBER_OF_EMPLOYEES_FIELD_NAME = 'numberOfEmployees';

    /**
     * @var Collection
     */
    private $collection;


    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param int|null $size
     */
    public function updateSize(string $ogrn, $size)
    {
        $this->updateFiledByOgrn(self::SIZE_FIELD_NAME, $ogrn, $size);
    }

    /**
     * @param mixed $value
     */
    private function updateFiledByOgrn(string $field, string $ogrn, $value)
    {
        if ($value) {
            $update = ['$set' => [$field => $value]];
        } else {
            $update = ['$unset' => [$field => '']];
        }

        $this->collection->updateOne(['ogrn' => $ogrn], $update);
    }

    /**
     * @param array|null $taxation
     */
    public function updateTaxation(string $ogrn, $taxation)
    {
        $this->updateFiledByOgrn(self::TAXATION_FIELD_NAME, $ogrn, $taxation);
    }


    /**
     * @param array[] $numberOfEmployeesItems
     */
    public function updateNumberOfEmployees(string $ogrn, array $numberOfEmployeesItems)
    {
        $this->updateFiledByOgrn(self::NUMBER_OF_EMPLOYEES_FIELD_NAME, $ogrn, $numberOfEmployeesItems);
    }
}