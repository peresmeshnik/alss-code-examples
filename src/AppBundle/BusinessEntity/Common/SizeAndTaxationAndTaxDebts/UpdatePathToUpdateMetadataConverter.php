<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Exception\InvalidArgumentException;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;
use AppBundle\Common\Util\RegExp;

abstract class UpdatePathToUpdateMetadataConverter
{
    const DATA_DATE_REGEXP = '/data-(\d{8})-structure/';
    const DEFAULT_DATA_DATE_FORMAT = 'Ymd';

    /**
     * @var UpdateFilenameValidator
     */
    private $updateFilenameValidator;

    /**
     * @var string
     */
    private $dataDateFormat;


    public function __construct(UpdateFilenameValidator $updateFilenameValidator)
    {
        $this->updateFilenameValidator = $updateFilenameValidator;
        $this->dataDateFormat = static::DEFAULT_DATA_DATE_FORMAT;
    }

    public function convert(string $updatePath): UpdateMetadata
    {
        $updateFilename = basename($updatePath);

        if (!$this->updateFilenameValidator->validate($updateFilename)) {
            throw new InvalidArgumentException(
                sprintf('Invalid update filename "%s".', $updateFilename)
            );
        }

        $dataDate = $this->parseDataDateFrom($updateFilename);

        return $this->createUpdateMetadataFrom($updateFilename, $dataDate);
    }

    private function parseDataDateFrom(string $updateFilename): \DateTime
    {
        $updateFilenameDate = (new RegExp(self::DATA_DATE_REGEXP))->getFirstCapturingGroup($updateFilename);

        return \DateTime::createFromFormat($this->dataDateFormat, $updateFilenameDate);
    }

    abstract protected function createUpdateMetadataFrom(string $updateFilename, \DateTime $dataDate);
}