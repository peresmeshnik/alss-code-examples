<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\Core\Store\ClearableStore;

interface UpdateEntryStore extends ClearableStore
{
    public function add($entity);
}