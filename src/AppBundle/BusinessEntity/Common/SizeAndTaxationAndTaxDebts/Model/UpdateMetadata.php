<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model;

use AppBundle\BusinessEntity\TaxDebts\UpdatePathToUpdateMetadataConverter;
use AppBundle\Document\UpdateMetadataType;
use MongoDB\BSON\Persistable;

class UpdateMetadata implements Persistable
{
    /**
     * @var string
     */
    public $updateName;

    /**
     * @var \DateTime
     */
    public $dataDate;

    /**
     * @var string
     */
    public $type;


    private function __construct(string $updateName, string $dateDate)
    {
        $this->updateName = $updateName;
        $this->dataDate = $dateDate;
    }

    public static function createSpecialTaxationRegimesUpdateMetadataWithUpdateNameAndDataDate(
        string $updateName,
        string $dateDate
    ): self {
        $result = new self($updateName, $dateDate);
        $result->type = UpdateMetadataType::SPECIAL_TAXATION_REGIMES_UPDATE;

        return $result;
    }

    public static function createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
        string $updateName,
        string $dateDate
    ): self {
        $result = new self($updateName, $dateDate);
        $result->type = UpdateMetadataType::SMALL_MIDDLE_ENTITY_UPDATE;

        return $result;
    }

    public static function createTaxDebtsUpdateMetadataFromUpdateNameAndDataDate(
        string $updateName,
        string $dateDate
    ): self {
        $result = new self($updateName, $dateDate);
        $result->type = UpdateMetadataType::TAX_DEBTS_UPDATE;

        return $result;
    }

    function bsonSerialize()
    {
        return [
            'updateName' => $this->updateName,
            'dataDate' => $this->dataDate,
            'type' => $this->type,
        ];
    }

    function bsonUnserialize(array $data)
    {
        $this->updateName = $data['updateName'];
        $this->dataDate = $data['dataDate'];
        $this->type = $data['type'];
    }
}