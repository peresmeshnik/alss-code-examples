<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use AppBundle\Core\UpdateImporting\ZipItemsTraverser;

class ZipUpdateParser
{
    /**
     * @var ZipItemsTraverser
     */
    private $zipItemsTraverser;

    /**
     * @var XmlUpdateParser
     */
    private $xmlUpdateParser;

    /**
     * @var UpdateEntryStore
     */
    private $updateEntryStore;

    /**
     * @var ZipUpdateParserReporter
     */
    private $reporter;

    /**
     * @var bool
     */
    private $clearUpdateEntryStore = true;


    public function __construct(
        ZipItemsTraverser $zipItemsTraverser,
        XmlUpdateParser $xmlUpdateParser,
        UpdateEntryStore $updateEntryStore,
        ZipUpdateParserReporter $reporter
    ) {
        $this->zipItemsTraverser = $zipItemsTraverser;
        $this->xmlUpdateParser = $xmlUpdateParser;
        $this->updateEntryStore = $updateEntryStore;
        $this->reporter = $reporter;
    }

    public function parse(string $zipUpdatePath)
    {
        $this->clearUpdateEntryStore();
        $this->zipItemsTraverser->setItemHandler(function ($xmlUpdatePath) {
            $this->parseXmlUpdate($xmlUpdatePath);
        });
        $this->zipItemsTraverser->traverse([$zipUpdatePath]);
    }

    private function clearUpdateEntryStore()
    {
        if ($this->clearUpdateEntryStore) {
            $this->reporter->reportClearingStart();
            $this->updateEntryStore->clear();
        }
    }

    private function parseXmlUpdate(string $xmlUpdatePath)
    {
        $this->saveAll(
            $this->xmlUpdateParser->parse(file_get_contents($xmlUpdatePath))
        );
    }

    /**
     * @param SmallBusiness[] $smallMiddleEntities
     */
    private function saveAll(array $smallMiddleEntities)
    {
        foreach ($smallMiddleEntities as $smallMiddleEntity) {
            $this->updateEntryStore->add($smallMiddleEntity);
        }
    }

    public function disableUpdateEntryStoreClearing()
    {
        $this->clearUpdateEntryStore = false;
    }
}