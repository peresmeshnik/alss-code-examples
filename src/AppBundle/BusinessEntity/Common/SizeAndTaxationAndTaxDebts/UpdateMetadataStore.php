<?php

namespace AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;

interface UpdateMetadataStore
{
    public function replaceWith(UpdateMetadata $metadata);

    /**
     * @return UpdateMetadata|null
     */
    public function get();
}