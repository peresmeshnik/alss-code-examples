<?php

namespace AppBundle\BusinessEntity\Common\Transformer;

use AppBundle\CompanyInfo\PathableDocument;

interface Transformer
{
    /**
     * @param PathableDocument[] $journalItems
     * @return PathableDocument[]
     */
    public function transform(array $journalItems): array;
}