<?php

namespace AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal;

use AppBundle\BusinessEntity\Common\Transformer\Transformer;
use AppBundle\Common\Util\Util;
use AppBundle\CompanyInfo\ArrayDocument;

class CompositeTransformer implements Transformer
{
    const EGRUL_JOURNAL_PATH = 'egrul.СвЗапЕГРЮЛ';
    const EGRIP_JOURNAL_PATH = 'egrip.СвЗапЕГРИП';

    /**
     * @var Transformer[]
     */
    private $transformers;

    /**
     * @var string;
     */
    private $journalPath;


    public function __construct(array $transformers)
    {
        $this->transformers = $transformers;
    }

    /**
     * @param Transformer[] $transformers
     */
    public static function createEgrulJournalCompositeTransformer(array $transformers): self
    {
        $result = new self($transformers);
        $result->journalPath = self::EGRUL_JOURNAL_PATH;

        return $result;
    }

    public static function createEgripJournalCompositeTransformer(array $transformers): self
    {
        $result = new self($transformers);
        $result->journalPath = self::EGRIP_JOURNAL_PATH;

        return $result;
    }

    public function transform(array $businessEntity): array
    {
        $transformedBusinessEntity = new ArrayDocument($businessEntity);
        
        if (!$transformedBusinessEntity->doesPathExist($this->journalPath)) {
            return $businessEntity;
        }
        
        $journalItems = $transformedBusinessEntity->getMultipleDocumentsByPath($this->journalPath);

        foreach ($this->transformers as $transformer) {
            $journalItems = $transformer->transform($journalItems);
        }

        $transformedBusinessEntity->setPathValueIfNotNull(
            $this->journalPath,
            Util::convertPathableDocumentsToArrays($journalItems)
        );

        return $transformedBusinessEntity->toArray();
    }
}