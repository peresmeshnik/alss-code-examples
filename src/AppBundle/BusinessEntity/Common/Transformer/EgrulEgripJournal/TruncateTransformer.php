<?php

namespace AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal;

use AppBundle\BusinessEntity\Common\Transformer\Transformer;
use AppBundle\CompanyInfo\PathableDocument;

class TruncateTransformer implements Transformer
{
    /**
     * @var int
     */
    private $length;


    public function __construct(int $length = 5)
    {
        $this->length = $length;
    }

    /**
     * @param PathableDocument[] $journalItems
     * @return PathableDocument[]
     */
    public function transform(array $journalItems): array
    {
        return array_slice($journalItems, 0, $this->length);
    }
}