<?php

namespace AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal;

use AppBundle\BusinessEntity\Common\Transformer\Transformer;
use AppBundle\Common\Util\Util;
use AppBundle\CompanyInfo\PathableDocument;

class NormalizeJournalDocumentsTransformer implements Transformer
{
    const DOCUMENTS_PATH = 'СведПредДок';

    /**
     * @inheritdoc
     */
    public function transform(array $journalItems): array
    {
        $result = $journalItems;

        /** @var PathableDocument $journalItem */
        foreach ($journalItems as &$journalItem) {
            if (!$journalItem->doesPathExist(self::DOCUMENTS_PATH)) {
                continue;
            }

            $journalItem->setPathValueIfNotNull(
                self::DOCUMENTS_PATH,
                Util::convertPathableDocumentsToArrays(
                    $journalItem->getMultipleDocumentsByPath(self::DOCUMENTS_PATH)
                )
            );
        }

        return $result;
    }
}