<?php

namespace AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal;

use AppBundle\BusinessEntity\Common\Transformer\Transformer;
use AppBundle\CompanyInfo\PathableDocument;

class SortDescByItemDateTransformer implements Transformer
{
    const JOURNAL_ITEM_DATE_PATH = '@attributes.ДатаЗап';

    /**
     * @param PathableDocument[] $journalItems
     * @return PathableDocument[]
     */
    public function transform(array $journalItems): array
    {
        $result = $journalItems;

        usort($result, function (PathableDocument $a, PathableDocument $b) {
            return -1 * strcmp(
                $a->getValueByPathOrThrow(self::JOURNAL_ITEM_DATE_PATH),
                $b->getValueByPathOrThrow(self::JOURNAL_ITEM_DATE_PATH)
            );
        });

        return $result;
    }
}