<?php

namespace AppBundle\BusinessEntity\Common;

use AppBundle\Common\Util\Util;
use MongoDB\Collection;

class SeeAlsoStore
{
    const DEFAULT_OPTIONS = [
        'typeMap' =>[
            'document' => 'array',
            'root' => 'array'
        ]
    ];

    /**
     * @var Collection
     */
    private $collection;


    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return string|null
     */
    public function getByOgrnAsJson(string $ogrn)
    {
        $current = $this->collection->findOne(
            [
                'ogrn' => $ogrn
            ],
            self::DEFAULT_OPTIONS
        );

        return Util::convertToJson(
            $this->getSeeAlsoBusinessEntitiesFor($current)
        );
    }

    public function getByOgrn(string $ogrn): array
    {
        $current = $this->collection->findOne(
            [
                'ogrn' => $ogrn
            ],
            self::DEFAULT_OPTIONS
        );

        return  $this->getSeeAlsoBusinessEntitiesFor($current);
    }

    private function getSeeAlsoBusinessEntitiesFor(array $current): array
    {
        $filter = [
            'address.regionCode' => $current['address']['regionCode'] ?? null,
            'economicActivity.main.code' => $current['economicActivity']['main']['code'] ?? null,
            'type' => $current['type'],
            'isDissolved' => false,
            'ogrn' => [
                '$ne' => $current['ogrn']
            ]
        ];
        $result =  $this->collection->find(
            $filter,
            array_merge(
                self::DEFAULT_OPTIONS,
                [
                    'skip' => (int) (Util::getRandomNumber() * 8),
                    'limit' => 5
                ]
            )
        );

        return iterator_to_array($result);
    }

}