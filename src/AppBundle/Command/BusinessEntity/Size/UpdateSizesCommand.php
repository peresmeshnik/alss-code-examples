<?php

namespace AppBundle\Command\BusinessEntity\Size;

use AppBundle\BusinessEntity\Size\BusinessEntity\UpdateSizesCommand as UpdateBusinessEntitiesSizeCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateSizesCommand extends Command
{
    /**
     * @var UpdateBusinessEntitiesSizeCommand
     */
    private $updateBusinessEntitiesSizeCommand;


    public function __construct(
        UpdateBusinessEntitiesSizeCommand $updateBusinessEntitiesSizeCommand
    ) {
        parent::__construct();

        $this->updateBusinessEntitiesSizeCommand = $updateBusinessEntitiesSizeCommand;
    }

    protected function configure()
    {
        $this
            ->setName('app:business-entity:size:update')
            ->setDescription('Updates existing business entities sizes.')
            ->setHelp(<<<EOT
The <info>%command.name%</info> command updates companies and entrepreneurs sizes in <info>torgbox_business_entities</info>
MongoDB collection using <info>small_businesses</info> collection.

Before running this command populate <info>small_businesses</info> collection with
<info>app:business-entity:size:parse-zip-update</info> command.
EOT
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->updateBusinessEntitiesSizeCommand->run();
    }
}