<?php

namespace AppBundle\Command\BusinessEntity\Size;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\ParseZipUpdateCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseSmallBusinessesZipUpdateCommand extends Command
{
    const ARGUMENT_NAME_ZIP_UPDATE_PATH = 'path';

    /**
     * @var ParseZipUpdateCommand
     */
    private $parseZipUpdateCommand;


    public function __construct(ParseZipUpdateCommand $parseZipUpdateCommand) {
        parent::__construct();

        $this->parseZipUpdateCommand = $parseZipUpdateCommand;
    }

    protected function configure()
    {
        $this
            ->setName('app:business-entity:size:parse-zip-update')
            ->setDescription('Parses small-middle entities ZIP update')
            ->setHelp(<<<EOT
EOT
            )
            ->addArgument(
                self::ARGUMENT_NAME_ZIP_UPDATE_PATH,
                InputArgument::REQUIRED,
                'ZIP update path'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->parseZipUpdateCommand->run(
            $input->getArgument(self::ARGUMENT_NAME_ZIP_UPDATE_PATH)
        );
    }
}