<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateEntryStore;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\XmlUpdateParser;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\ZipUpdateParser;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\ZipUpdateParserReporter;
use AppBundle\Core\UpdateImporting\ZipItemsTraverser;
use PHPUnit\Framework\TestCase;

class ZipUpdateParserTest extends TestCase
{
    public function testUpdateEntryStoreClearing_enabledByDefault()
    {
        $mockUpdateEntryStore = $this->createMock(UpdateEntryStore::class);
        $mockUpdateEntryStore->expects($this->once())
            ->method('clear')
        ;
        $sut = $this->makeZipUpdateParserWithUpdateEntryStore($mockUpdateEntryStore);

        $sut->parse('');
    }

    private function makeZipUpdateParserWithUpdateEntryStore(UpdateEntryStore $updateEntryStore): ZipUpdateParser
    {
        $dummyZipItemsTraverser = $this->createMock(ZipItemsTraverser::class);
        $dummyXmlUpdateParser = $this->createMock(XmlUpdateParser::class);
        $dummyZipUpdateParseReporter = $this->createMock(ZipUpdateParserReporter::class);

        return new ZipUpdateParser(
            $dummyZipItemsTraverser,
            $dummyXmlUpdateParser,
            $updateEntryStore,
            $dummyZipUpdateParseReporter
        );
    }

    public function testUpdateEntryStoreClearing_canBeDisabled()
    {
        $mockUpdateEntryStore = $this->createMock(UpdateEntryStore::class);
        $mockUpdateEntryStore->expects($this->never())
            ->method('clear')
        ;
        $sut = $this->makeZipUpdateParserWithUpdateEntryStore($mockUpdateEntryStore);

        $sut->disableUpdateEntryStoreClearing();
        $sut->parse('');
    }
}
