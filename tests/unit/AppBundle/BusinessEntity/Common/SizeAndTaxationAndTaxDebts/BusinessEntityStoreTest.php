<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\BusinessEntityStore;
use AppBundle\BusinessEntity\Taxation\BusinessEntity\TaxationRegime;
use MongoDB\Collection;
use PHPUnit\Framework\TestCase;

class BusinessEntityStoreTest extends TestCase
{
    public function testUpdatesSizeField_sizeIsNotNull_setsSizeField()
    {
        $expectedOgrn = '1';
        $expectedSize = '2';
        $mockCollection = $this->createMock(Collection::class);
        $sut = new BusinessEntityStore($mockCollection);

        $mockCollection->expects($this->once())
            ->method('updateOne')
            ->with(['ogrn' => $expectedOgrn], ['$set' => [BusinessEntityStore::SIZE_FIELD_NAME => $expectedSize]])
        ;

        $sut->updateSize($expectedOgrn, $expectedSize);
    }

    public function testUpdatesSizeField_sizeIsNull_unsetsSizeField()
    {
        $expectedOgrn = '1';
        $expectedSize = null;
        $mockCollection = $this->createMock(Collection::class);
        $sut = new BusinessEntityStore($mockCollection);

        $mockCollection->expects($this->once())
            ->method('updateOne')
            ->with(['ogrn' => $expectedOgrn], ['$unset' => [BusinessEntityStore::SIZE_FIELD_NAME => '']])
        ;

        $sut->updateSize($expectedOgrn, $expectedSize);
    }

    public function testUpdatesTaxationField_taxationIsNotNull_setsTaxation()
    {
        $ogrn = '1';
        $taxation = [TaxationRegime::USN, TaxationRegime::SRP];
        $mockCollection = $this->createMock(Collection::class);
        $sut = new BusinessEntityStore($mockCollection);

        $mockCollection->expects($this->once())
            ->method('updateOne')
            ->with(['ogrn' => $ogrn], ['$set' => [BusinessEntityStore::TAXATION_FIELD_NAME => $taxation]])
        ;

        $sut->updateTaxation($ogrn, $taxation);
    }

    public function testUpdatesSizeField_taxationIsNull_unsetsTaxation()
    {
        $ogrn = '1';
        $taxation = null;
        $mockCollection = $this->createMock(Collection::class);
        $sut = new BusinessEntityStore($mockCollection);

        $mockCollection->expects($this->once())
            ->method('updateOne')
            ->with(['ogrn' => $ogrn], ['$unset' => [BusinessEntityStore::TAXATION_FIELD_NAME => '']])
        ;

        $sut->updateTaxation($ogrn, $taxation);
    }
}
