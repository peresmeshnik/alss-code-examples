<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Common\Transformer;

use AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal\SortDescByItemDateTransformer;
use AppBundle\CompanyInfo\ArrayDocument;
use PHPUnit\Framework\TestCase;

class SortDescByItemDateTransformerTest extends TestCase
{
    public function testSorting()
    {
        $journal = [
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-04-25'
                ]
            ]),
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-05-01'
                ]
            ])
        ];
        $expectedSortedJournal = [
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-05-01'
                ]
            ]),
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-04-25'
                ]
            ])
        ];

        $sut = new SortDescByItemDateTransformer();

        $actual = $sut->transform($journal);

        $this->assertEquals($expectedSortedJournal, $actual);
    }
}
