<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Common\Transformer;

use AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal\TruncateTransformer;
use AppBundle\CompanyInfo\ArrayDocument;
use PHPUnit\Framework\TestCase;

class TruncateTransformerTest extends TestCase
{
    public function testTrimming()
    {
        $journalItems = [
            new ArrayDocument(['1' => '1']),
            new ArrayDocument(['2' => '2']),
        ];
        $sut = new TruncateTransformer(1);

        $actual = $sut->transform($journalItems);

        $this->assertCount(1, $actual);
    }
}
