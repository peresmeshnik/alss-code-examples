<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal;

use AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal\NormalizeJournalDocumentsTransformer;
use AppBundle\CompanyInfo\ArrayDocument;
use PHPUnit\Framework\TestCase;

class NormalizeJournalDocumentsTransformerTest extends TestCase
{
    public function testNormalizing_oneDocument_toArrayOfOneDocument()
    {
        $journal = [
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-04-25'
                ],
                'СведПредДок' => [
                    'a' => 'a'
                ]
            ])
        ];
        $expectedJournalWithNormalizedDocuments = [
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-04-25'
                ],
                'СведПредДок' => [
                    [
                        'a' => 'a'
                    ]
                ]
            ])
        ];
        $sut = new NormalizeJournalDocumentsTransformer();

        $actual = $sut->transform($journal);

        $this->assertEquals($expectedJournalWithNormalizedDocuments, $actual);
    }

    public function testNormalizing_multipleJournalItems_noDocumentsInFirstItem()
    {
        $journal = [
            new ArrayDocument(['a' => 'a']),
            new ArrayDocument([
                'СведПредДок' => [
                    'b' => 'b'
                ]
            ])
        ];
        $expectedJournalWithNormalizedDocuments = [
            new ArrayDocument(['a' => 'a']),
            new ArrayDocument([
                'СведПредДок' => [
                    [
                        'b' => 'b'
                    ]
                ]
            ])
        ];
        $sut = new NormalizeJournalDocumentsTransformer();

        $actual = $sut->transform($journal);

        $this->assertEquals($expectedJournalWithNormalizedDocuments, $actual);
    }

    public function testNormalizing_doesNothingIfNoDocuments()
    {
        $expectedJournal = [
            new ArrayDocument([
                'a' => 'a'
            ])
        ];
        $sut = new NormalizeJournalDocumentsTransformer();

        $actual = $sut->transform($expectedJournal);

        $this->assertEquals($expectedJournal, $actual);
    }

    public function testNormalizing_multipleDocuments_doesNothing()
    {
        $expectedJournal = [
            new ArrayDocument([
                '@attributes' => [
                    'ДатаЗап' => '2007-04-25'
                ],
                'СведПредДок' => [
                    [
                        'a' => 'a'
                    ],
                    [
                        'b' => 'b'
                    ]
                ]
            ])
        ];
        $sut = new NormalizeJournalDocumentsTransformer();

        $actual = $sut->transform($expectedJournal);

        $this->assertEquals($expectedJournal, $actual);
    }
}