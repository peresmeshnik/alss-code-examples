<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal;

use AppBundle\BusinessEntity\Common\Transformer\EgrulEgripJournal\CompositeTransformer;
use AppBundle\BusinessEntity\Common\Transformer\Transformer;
use AppBundle\CompanyInfo\PathableDocument;
use PHPUnit\Framework\TestCase;

class CompositeTransformerTest extends TestCase
{
    const COMPANY = [
        'egrul' => [
            'СвЗапЕГРЮЛ' => [
                [
                    'a' => 'b'
                ]
            ]
        ]
    ];
    const TRANSFORMED_COMPANY = [
        'egrul' => [
            'СвЗапЕГРЮЛ' => [
                [
                    'a' => 'c'
                ]
            ]
        ]
    ];

    public function testEgrulJournalTransforming()
    {
        $sut = CompositeTransformer::createEgrulJournalCompositeTransformer([new FakeTransformer]);

        $actual = $sut->transform(self::COMPANY);

        $this->assertEquals(self::TRANSFORMED_COMPANY, $actual);
    }

    const ENTREPRENEUR = [
        'egrip' => [
            'СвЗапЕГРИП' => [
                [
                    'a' => 'b'
                ]
            ]
        ]
    ];
    const TRANSFORMED_ENTREPRENEUR = [
        'egrip' => [
            'СвЗапЕГРИП' => [
                [
                    'a' => 'c'
                ]
            ]
        ]
    ];

    public function testEgripJournalTransforming()
    {
        $sut = CompositeTransformer::createEgripJournalCompositeTransformer([new FakeTransformer]);

        $actual = $sut->transform(self::ENTREPRENEUR);

        $this->assertEquals(self::TRANSFORMED_ENTREPRENEUR, $actual);
    }

    const BUSINESS_ENTITY_WITHOUT_JOURNAL = [
        'ogrn' => '1'
    ];

    public function testTransforming_egrul_doesNothingIfNoJournal()
    {
        $sut = CompositeTransformer::createEgrulJournalCompositeTransformer([new FakeTransformer]);

        $actual = $sut->transform(self::BUSINESS_ENTITY_WITHOUT_JOURNAL);

        $this->assertEquals(self::BUSINESS_ENTITY_WITHOUT_JOURNAL, $actual);
    }

    public function testTransforming_egrip_doesNothingIfNoJournal()
    {
        $sut = CompositeTransformer::createEgripJournalCompositeTransformer([new FakeTransformer]);

        $actual = $sut->transform(self::BUSINESS_ENTITY_WITHOUT_JOURNAL);

        $this->assertEquals(self::BUSINESS_ENTITY_WITHOUT_JOURNAL, $actual);
    }
}

class FakeTransformer implements Transformer
{
    /**
     * @param PathableDocument[] $journal
     * @return PathableDocument[]
     */
    public function transform(array $journal): array
    {
        $journal[0]->setPathValueIfNotNull('a', 'c');

        return $journal;
    }
}


