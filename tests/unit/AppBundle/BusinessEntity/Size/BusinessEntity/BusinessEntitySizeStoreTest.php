<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Size\BusinessEntity;

use AppBundle\BusinessEntity\Size\BusinessEntity\BusinessEntitySizeStore;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\RecentBusinessEntityStore;
use AppBundle\BusinessEntity\Size\BusinessEntity\Size;
use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use AppBundle\BusinessEntity\Size\SmallBusiness\SmallBusinessStore;
use PHPUnit\Framework\TestCase;

class BusinessEntitySizeStoreTest extends TestCase
{
    public function testReturnsSize_isNotSmallBusiness_recentlyCreated_returnsNull()
    {
        $stubSmallBusinessStore = $this->createMock(SmallBusinessStore::class);
        $stubSmallBusinessStore->method('getOneByInn')
            ->willReturn(null)
        ;
        $stubRecentBusinessEntityStore = $this->createMock(RecentBusinessEntityStore::class);
        $stubRecentBusinessEntityStore->method('contains')
            ->willReturn(true)
        ;
        $sut = new BusinessEntitySizeStore($stubSmallBusinessStore, $stubRecentBusinessEntityStore);

        $actual = $sut->getFor(['isDissolved' => false]);

        $this->assertNull($actual);
    }

    public function testReturnsSize_isNotSmallBusinesses_isNotRecentlyCreated_returnsLargeSize()
    {
        $stubSmallBusinessStore = $this->createMock(SmallBusinessStore::class);
        $stubSmallBusinessStore->method('getOneByInn')
            ->willReturn(null)
        ;
        $stubRecentBusinessEntityStore = $this->createMock(RecentBusinessEntityStore::class);
        $stubRecentBusinessEntityStore->method('contains')
            ->willReturn(false)
        ;
        $sut = new BusinessEntitySizeStore($stubSmallBusinessStore, $stubRecentBusinessEntityStore);

        $actual = $sut->getFor([
            'isDissolved' => false,
            'regDate' => true
        ]);

        $this->assertEquals(Size::LARGE, $actual);
    }

    public function testReturnsSize_isSmallBusinesses_notRecentlyCreated_returnsSmallBusinessSize()
    {
        $company = [
            'inn' => '123',
            'isDissolved' => false
        ];
        $stubSmallBusinessStore = $this->createMock(SmallBusinessStore::class);
        $stubSmallBusinessStore->method('getOneByInn')
            ->willReturn(
                SmallBusiness::aMicro()
                    ->company()
                    ->withInn($company['inn'])
            )
        ;
        $stubRecentBusinessEntityStore = $this->createMock(RecentBusinessEntityStore::class);
        $stubRecentBusinessEntityStore->method('contains')
            ->willReturn(false)
        ;
        $sut = new BusinessEntitySizeStore($stubSmallBusinessStore, $stubRecentBusinessEntityStore);

        $actual = $sut->getFor($company);

        $this->assertEquals(Size::MICRO, $actual);
    }

    public function testReturnsSize_isSmallBusiness_recentlyCreated_returnsSmallBusinessSize()
    {
        $company = [
            'inn' => '123',
            'isDissolved' => false
        ];
        $stubSmallBusinessStore = $this->createMock(SmallBusinessStore::class);
        $stubSmallBusinessStore->method('getOneByInn')
            ->willReturn(
                SmallBusiness::aSmall()
                    ->company()
                    ->withInn($company['inn'])
            )
        ;
        $stubRecentBusinessEntityStore = $this->createMock(RecentBusinessEntityStore::class);
        $stubRecentBusinessEntityStore->method('contains')
            ->willReturn(true)
        ;
        $sut = new BusinessEntitySizeStore($stubSmallBusinessStore, $stubRecentBusinessEntityStore);

        $actual = $sut->getFor($company);

        $this->assertEquals(Size::SMALL, $actual);
    }

    public function testReturnsSize_isNotSmallBusiness_isDissolved_returnsNull()
    {
        $dummySmallBusinessesStore = $this->createMock(SmallBusinessStore::class);
        $dummyRecentBusinessEntityStore = $this->createMock(RecentBusinessEntityStore::class);
        $sut = new BusinessEntitySizeStore($dummySmallBusinessesStore, $dummyRecentBusinessEntityStore);

        $actual = $sut->getFor(['isDissolved' => true]);

        $this->assertNull($actual);
    }

    public function testReturnsSize_isNotSmallBusiness_noRegDate_returnsNull()
    {
        $dummySmallBusinessesStore = $this->createMock(SmallBusinessStore::class);
        $dummyRecentBusinessEntityStore = $this->createMock(RecentBusinessEntityStore::class);
        $sut = new BusinessEntitySizeStore($dummySmallBusinessesStore, $dummyRecentBusinessEntityStore);

        $actual = $sut->getFor([
            'isDissolved' => false,
        ]);

        $this->assertNull($actual);
    }
}
