<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Size\BusinessEntity;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateMetadataStore;
use AppBundle\BusinessEntity\Size\BusinessEntity\Exception\UnexpectedValueException;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\RecentBusinessEntityStore;
use PHPUnit\Framework\TestCase;

class RecentBusinessEntityStoreTest extends TestCase
{
    public function testContains_createdWithinNDaysBeforeLastUpdateDataDate_true()
    {
        $nDays = 30;
        $updateDataDate = '2019-05-24';
        $businessEntity = [
            'regDate' => '2019-04-28'
        ];
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate('', $updateDataDate);
        $stubUpdateMetadataStore = $this->createMock(UpdateMetadataStore::class);
        $stubUpdateMetadataStore->method('get')->willReturn($updateMetadata);
        $sut = new RecentBusinessEntityStore($stubUpdateMetadataStore, $nDays);

        $actual = $sut->contains($businessEntity);

        $this->assertTrue($actual);
    }

    public function testContains_createdAfterLastUpdateDataDate_true()
    {
        $updateDataDate = '2019-05-25';
        $businessEntity = [
            'regDate' => '2019-05-24'
        ];
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate('', $updateDataDate);
        $stubUpdateMetadataStore = $this->createMock(UpdateMetadataStore::class);
        $stubUpdateMetadataStore->method('get')->willReturn($updateMetadata);
        $sut = new RecentBusinessEntityStore($stubUpdateMetadataStore);

        $actual = $sut->contains($businessEntity);

        $this->assertTrue($actual);
    }

    public function testContains_createdMoreThanNDaysBeforeLastUpdateDataDate_false()
    {
        $nDays = 30;
        $updateDataDate = '2019-05-25';
        $businessEntity = [
            'regDate' => '2019-04-24'
        ];
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate('', $updateDataDate);
        $stubUpdateMetadataStore = $this->createMock(UpdateMetadataStore::class);
        $stubUpdateMetadataStore->method('get')->willReturn($updateMetadata);
        $sut = new RecentBusinessEntityStore($stubUpdateMetadataStore, $nDays);

        $actual = $sut->contains($businessEntity);

        $this->assertFalse($actual);
    }

    public function testContains_noUpdateMetadata_throwException()
    {
        $stubUpdateMetadataStore = $this->createMock(UpdateMetadataStore::class);
        $stubUpdateMetadataStore->method('get')->willReturn(null);
        $sut = new RecentBusinessEntityStore($stubUpdateMetadataStore);

        $this->expectException(UnexpectedValueException::class);
        $this->expectExceptionMessage('No update');

        $sut->contains([]);
    }
}
