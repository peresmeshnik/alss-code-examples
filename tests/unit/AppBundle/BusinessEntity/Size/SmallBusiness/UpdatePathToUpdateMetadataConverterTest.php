<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Size;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Exception\InvalidArgumentException;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateFilenameValidator;
use AppBundle\BusinessEntity\Size\SmallBusiness\UpdatePathToUpdateMetadataConverter;
use PHPUnit\Framework\TestCase;

class UpdatePathToUpdateMetadataConverterTest extends TestCase
{
    const TEST_UPDATE_PATH = '/home/data-05102019-structure-08012016.zip';

    public function testConverting()
    {
        $expectedUpdateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            'data-05102019-structure-08012016.zip',
            '2019-05-10'
        );
        $stubUpdateFilenameValidator = $this->createMock(UpdateFilenameValidator::class);
        $stubUpdateFilenameValidator->method('validate')->willReturn(true);
        $sut = new UpdatePathToUpdateMetadataConverter($stubUpdateFilenameValidator);

        $actual = $sut->convert(self::TEST_UPDATE_PATH);

        $this->assertEquals($expectedUpdateMetadata, $actual);
    }

    const INVALID_UPDATE_FILENAME = '/home/123data-05102019-structure-08012016.zip';

    public function testConverting_invalidUpdateFilename_throwsException()
    {
        $stubUpdateFilenameValidator = $this->createMock(UpdateFilenameValidator::class);
        $stubUpdateFilenameValidator->method('validate')->willReturn(false);
        $sut = new UpdatePathToUpdateMetadataConverter($stubUpdateFilenameValidator);

        $this->expectException(InvalidArgumentException::class);

        $sut->convert(self::TEST_UPDATE_PATH);
    }
}
