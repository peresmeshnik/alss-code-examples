<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Size\BusinessEntity;

use AppBundle\BusinessEntity\Size\SmallBusiness\Exception\RuntimeException;
use AppBundle\BusinessEntity\Size\SmallBusiness\XmlUpdateParser;
use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use PHPUnit\Framework\TestCase;

class XmlUpdateParserTest extends TestCase
{
    const XML_UPDATE_WITH_ONE_MICRO_COMPANY = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="1" КатСубМСП="1">
        <ОргВклМСП ИННЮЛ="6318042035"/>
    </Документ>
</Файл>
XML;

    public function testParsing_microCompany()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aMicro()->company()->withInn('6318042035');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_ONE_MICRO_COMPANY);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }

    const XML_UPDATE_WITH_ONE_SMALL_COMPANY = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="1" КатСубМСП="2">
        <ОргВклМСП ИННЮЛ="6318042035"/>
    </Документ>
</Файл>
XML;

    public function testParsing_smallCompany()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aSmall()->company()->withInn('6318042035');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_ONE_SMALL_COMPANY);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }

    const XML_UPDATE_WITH_ONE_MIDDLE_COMPANY = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="1" КатСубМСП="3">
        <ОргВклМСП ИННЮЛ="6318042035"/>
    </Документ>
</Файл>
XML;

    public function testParsing_middleCompany()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aMiddle()->company()->withInn('6318042035');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_ONE_MIDDLE_COMPANY);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }

    const XML_UPDATE_WITH_ONE_MICRO_ENTREPRENEUR = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="2" КатСубМСП="1">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testParsing_microEntrepreneur()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aMicro()->entrepreneur()->withInn('631805182801');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_ONE_MICRO_ENTREPRENEUR);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }

    const XML_UPDATE_WITH_ONE_SMALL_ENTREPRENEUR = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="2" КатСубМСП="2">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testParsing_smallEntrepreneur()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aSmall()->entrepreneur()->withInn('631805182801');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_ONE_SMALL_ENTREPRENEUR);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }

    const XML_UPDATE_WITH_ONE_MIDDLE_ENTREPRENEUR = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="2" КатСубМСП="3">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testParsing_middleEntrepreneur()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aMiddle()->entrepreneur()->withInn('631805182801');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_ONE_MIDDLE_ENTREPRENEUR);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }

    const XML_UPDATE_WITH_UNKNOWN_SMALL_MIDDLE_ENTITY_TYPE = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="123" КатСубМСП="3">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testParsing_unknownSmallMiddleEntityType()
    {
        $sut = new XmlUpdateParser();

        $this->expectException(RuntimeException::class);

        $sut->parse(self::XML_UPDATE_WITH_UNKNOWN_SMALL_MIDDLE_ENTITY_TYPE);
    }

    const XML_UPDATE_WITH_UNKNOWN_SMALL_MIDDLE_ENTITY_CATEGORY = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="1" КатСубМСП="123">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testParsing_unknownSmallMiddleEntityCategory()
    {
        $sut = new XmlUpdateParser();

        $this->expectException(RuntimeException::class);

        $sut->parse(self::XML_UPDATE_WITH_UNKNOWN_SMALL_MIDDLE_ENTITY_CATEGORY);
    }

    const XML_UPDATE_WITH_COMPANY_AND_ENTREPRENEUR = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="1" КатСубМСП="1">
        <ОргВклМСП ИННЮЛ="6318042035"/>
    </Документ>
        <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="2" КатСубМСП="3">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testParsing_companyAndEntrepreneur()
    {
        $expectedSmallMiddleEntities[] = SmallBusiness::aMicro()->company()->withInn('6318042035');
        $expectedSmallMiddleEntities[] = SmallBusiness::aMiddle()->entrepreneur()->withInn('631805182801');
        $sut = new XmlUpdateParser();

        $actual = $sut->parse(self::XML_UPDATE_WITH_COMPANY_AND_ENTREPRENEUR);

        $this->assertEquals($expectedSmallMiddleEntities, $actual);
    }
}
