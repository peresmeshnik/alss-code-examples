<?php

namespace Tests\Unit\AppBundle\BusinessEntity\Size;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateFilenameValidator;
use PHPUnit\Framework\TestCase;

class UpdateFilenameValidatorTest extends TestCase
{
    const VALID_UPDATE_FILENAME = 'data-05102019-structure-08012016.zip';

    public function testValidating_valid()
    {
        $sut = new UpdateFilenameValidator('08012016');

        $actual = $sut->validate(self::VALID_UPDATE_FILENAME);

        $this->assertTrue($actual);
    }

    const INVALID_UPDATE_FILENAME = '123data-05102019-structure-08012016.zip';

    public function testValidating_invalid()
    {
        $sut = new UpdateFilenameValidator('08012016');

        $actual = $sut->validate(self::INVALID_UPDATE_FILENAME);

        $this->assertFalse($actual);
    }

    const UPDATE_FILENAME_WITH_WRONG_STRUCTURE_VERSION = 'data-05102019-structure-08012016.zip';
}
