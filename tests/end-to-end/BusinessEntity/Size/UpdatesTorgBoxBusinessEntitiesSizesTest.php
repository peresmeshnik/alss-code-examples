<?php

namespace Tests\EndToEnd\BusinessEntity\Size;

use AppBundle\BusinessEntity\Size\BusinessEntity\Size;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;
use AppBundle\BusinessEntity\Size\SmallBusiness\SmallBusinessStore;
use AppBundle\BusinessEntity\Size\SmallBusiness\UpdateMetadataStore;
use AppBundle\BusinessEntity\Size\BusinessEntity\UpdateSizesCommand;
use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use AppBundle\EntrepreneurInfo\EntrepreneurInfo;
use AppBundle\TorgBox\Store\BusinessEntityStore;
use Tests\EndToEnd\FullTextSearch\Helpers\Builders\EntrepreneurBuilder;
use Tests\Helpers\CompanyInfoBuilder;
use Tests\Helpers\TestWithDependencies;

class UpdatesTorgBoxBusinessEntitiesSizesTest extends TestWithDependencies
{
    /**
     * @var UpdateMetadataStore
     */
    protected $smallBusinessesUpdateMetadataStore;

    /**
     * @var BusinessEntityStore
     */
    protected $businessEntityStore;

    /**
     * @var UpdateSizesCommand
     */
    protected $updateSizesCommand;

    /**
     * @var SmallBusinessStore
     */
    protected $smallBusinessStore;


    public function dependencies(): array
    {
        return [
            'smallBusinessesUpdateMetadataStore' => UpdateMetadataStore::class,
            'businessEntityStore' => BusinessEntityStore::class,
            'updateSizesCommand' => UpdateSizesCommand::class,
            'smallBusinessStore' => SmallBusinessStore::class
        ];
    }

    protected function setUp()
    {
        $this->smallBusinessesUpdateMetadataStore->clear();
        $this->businessEntityStore->clear();
        $this->smallBusinessStore->clear();
    }

    protected function tearDown()
    {
        $this->smallBusinessesUpdateMetadataStore->clear();
        $this->businessEntityStore->clear();
        $this->smallBusinessStore->clear();
    }

    public function testUpdating_isNotSmallBusiness_recentlyCreated_removesSizeField()
    {
        $updateDataDate = '2019-05-24';
        $regDate = '2019-04-28';
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            '', $updateDataDate
        );
        $this->smallBusinessesUpdateMetadataStore->replaceWith($updateMetadata);
        $company = CompanyInfoBuilder::anEmptyCompanyInfo()
            ->withOgrn()
            ->withEstablishmentDate($regDate)
            ->withSize(Size::LARGE)
            ->build();
        $this->businessEntityStore->add($company);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($company->ogrn);

        $this->assertArrayNotHasKey('size', $company);
    }

    public function testUpdating_isNotSmallBusiness_isNotRecentlyCreated_setsLargeSize()
    {
        $updateDataDate = '2019-05-24';
        $regDate = '2019-04-20';
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            '',
            $updateDataDate
        );
        $this->smallBusinessesUpdateMetadataStore->replaceWith($updateMetadata);
        $company = CompanyInfoBuilder::anEmptyCompanyInfo()
            ->withOgrn()
            ->withEstablishmentDate($regDate)
            ->build()
        ;
        $this->businessEntityStore->add($company);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($company->ogrn);

        $this->assertEquals(Size::LARGE, $company['size']);
    }

    public function testUpdating_isSmallBusiness_isNotRecentlyCreated_updatesSize()
    {
        $updateDataDate = '2019-05-24';
        $regDate = '2019-04-20';
        $company = CompanyInfoBuilder::anEmptyCompanyInfo()
            ->withInn()
            ->withOgrn()
            ->withEstablishmentDate($regDate)
            ->withSize(Size::LARGE)
            ->build();
        $this->businessEntityStore->add($company);
        $smallMiddleBusinessEntity = SmallBusiness::aMicro()
            ->company()
            ->withInn($company->inn)
        ;
        $this->smallBusinessStore->add($smallMiddleBusinessEntity);
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            '',
            $updateDataDate
        );
        $this->smallBusinessesUpdateMetadataStore->replaceWith($updateMetadata);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($company->ogrn);

        $this->assertEquals(Size::MICRO, $company['size']);
    }

    public function testUpdating_isSmallBusiness_isRecentlyCreated_entrepreneur_updatesSize()
    {
        $updateDataDate = '2019-05-24';
        $regDate = '2019-04-20';
        $entrepreneur = [
            'inn' => '1',
            'ogrn' => '2',
            'regDate' => $regDate,
            'size' => Size::LARGE,
            'isDissolved' => false,
            'type' => 'entrepreneur'
        ];
        $this->businessEntityStore->addOrUpdateAsArray($entrepreneur);
        $smallMiddleBusinessEntity = SmallBusiness::aMicro()
            ->company()
            ->withInn($entrepreneur['inn'])
        ;
        $this->smallBusinessStore->add($smallMiddleBusinessEntity);
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            '',
            $updateDataDate
        );
        $this->smallBusinessesUpdateMetadataStore->replaceWith($updateMetadata);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($entrepreneur['ogrn']);

        $this->assertEquals(Size::MICRO, $company['size']);
    }

    public function testUpdating_isSmallBusiness_isRecentlyCreated_updatesSize()
    {
        $updateDataDate = '2019-05-24';
        $regDate = '2019-04-28';
        $company = CompanyInfoBuilder::anEmptyCompanyInfo()
            ->withInn()
            ->withOgrn()
            ->withEstablishmentDate($regDate)
            ->withSize(Size::SMALL)
            ->build();
        $this->businessEntityStore->add($company);
        $smallMiddleBusinessEntity = SmallBusiness::aMiddle()
            ->company()
            ->withInn($company->inn)
        ;
        $this->smallBusinessStore->add($smallMiddleBusinessEntity);
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            '',
            $updateDataDate
        );
        $this->smallBusinessesUpdateMetadataStore->replaceWith($updateMetadata);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($company->ogrn);

        $this->assertEquals(Size::MIDDLE, $company['size']);
    }

    public function testUpdating_noSizeField_isSmallBusiness_notRecentlyCreated_addsSizeField()
    {
        $updateDataDate = '2019-05-24';
        $regDate = '2019-04-20';
        $company = CompanyInfoBuilder::anEmptyCompanyInfo()
            ->withInn()
            ->withOgrn()
            ->withEstablishmentDate($regDate)
            ->build();
        $this->businessEntityStore->add($company);
        $smallMiddleBusinessEntity = SmallBusiness::aSmall()
            ->company()
            ->withInn($company->inn)
        ;
        $this->smallBusinessStore->add($smallMiddleBusinessEntity);
        $updateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            '',
            $updateDataDate
        );
        $this->smallBusinessesUpdateMetadataStore->replaceWith($updateMetadata);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($company->ogrn);

        $this->assertEquals(Size::SMALL, $company['size']);
    }

    public function testUpdating_isDissolved_isNotSmallBusiness_removesSizeField()
    {
        $ogrn = '1';
        $businessEntity = [
            'ogrn' => $ogrn,
            'inn' => '2',
            'size' => Size::SMALL,
            'isDissolved' => true,
            'type' => 'entrepreneur'
        ];
        $this->businessEntityStore->addOrUpdateAsArray($businessEntity);

        $this->updateSizesCommand->run();
        $businessEntity = $this->businessEntityStore->getByOgrnAsArray($businessEntity['ogrn']);

        $this->assertArrayNotHasKey('size', $businessEntity);
    }

    public function testUpdating_isDissolved_isSmallBusiness_updatesSize()
    {
        $ogrn = '1';
        $businessEntity = [
            'ogrn' => $ogrn,
            'inn' => '2',
            'size' => Size::MIDDLE,
            'isDissolved' => true,
            'type' => 'company'
        ];
        $this->businessEntityStore->addOrUpdateAsArray($businessEntity);
        $smallMiddleBusinessEntity = SmallBusiness::aSmall()
            ->company()
            ->withInn($businessEntity['inn'])
        ;
        $this->smallBusinessStore->add($smallMiddleBusinessEntity);

        $this->updateSizesCommand->run();
        $company = $this->businessEntityStore->getByOgrnAsArray($businessEntity['ogrn']);

        $this->assertEquals(Size::SMALL, $company['size']);
    }
}