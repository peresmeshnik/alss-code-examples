<?php

namespace Tests\EndToEnd\BusinessEntity\Size;

use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\ParseZipUpdateCommand;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\ZipUpdateParser;
use AppBundle\BusinessEntity\Size\SmallBusiness\Model\SmallBusiness;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\Model\UpdateMetadata;
use AppBundle\BusinessEntity\Size\SmallBusiness\ParseSmallBusinessesZipUpdateCommand;
use AppBundle\BusinessEntity\Size\SmallBusiness\SmallBusinessStore;
use AppBundle\BusinessEntity\Common\SizeAndTaxationAndTaxDebts\UpdateFilenameValidator;
use AppBundle\BusinessEntity\Size\SmallBusiness\UpdateMetadataStore;
use AppBundle\BusinessEntity\Size\SmallBusiness\UpdatePathToUpdateMetadataConverter;
use AppBundle\Common\Util\Path;
use Tests\EndToEnd\Helpers\Directory;
use Tests\EndToEnd\Helpers\Util;
use Tests\Helpers\TestWithDependencies;

class ParsesSmallBusinessesZipUpdateTest extends TestWithDependencies
{
    /**
     * @var Directory
     */
    private $tempDirectory;

    /**
     * @var string
     */
    private $zipUpdatePath;

    /**
     * @var ParseSmallBusinessesZipUpdateCommand
     */
    protected $parseSmallBusinessesZipUpdateCommand;

    /**
     * @var SmallBusinessStore
     */
    protected $smallBusinessStore;

    /**
     * @var UpdateMetadataStore
     */
    protected $updateMetadataStore;


    protected function dependencies(): array
    {
        return [
            'parseSmallBusinessesZipUpdateCommand' => 'app.business_entity.size.small_business.parse_zip_update_command',
            'smallBusinessStore' => SmallBusinessStore::class,
            'updateMetadataStore' => UpdateMetadataStore::class
        ];
    }

    protected function setUp()
    {
        $this->tempDirectory = new Directory(Path::join(__DIR__, '/tmp'));
        $this->zipUpdatePath = Path::join($this->tempDirectory->getPath(), 'data-05102019-structure-08012016.zip');
        $this->tempDirectory->clear();
        $this->smallBusinessStore->clear();
        $this->updateMetadataStore->clear();
    }

    protected function tearDown()
    {
        $this->tempDirectory->clear();
        $this->smallBusinessStore->clear();
        $this->updateMetadataStore->clear();
    }

    const XML_UPDATE_WITH_ONE_MICRO_COMPANY = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="1" КатСубМСП="1">
        <ОргВклМСП ИННЮЛ="6318042035"/>
    </Документ>
</Файл>
XML;

    public function testUpdateParsing_company()
    {
        $expectedCompany = SmallBusiness::aMicro()->company()->withInn('6318042035');
        Util::createZipArchiveWithFilesContents($this->zipUpdatePath, ['u.xml' => self::XML_UPDATE_WITH_ONE_MICRO_COMPANY]);

        $this->parseSmallBusinessesZipUpdateCommand->run($this->zipUpdatePath);
        $actual = $this->smallBusinessStore->getOneByInn($expectedCompany->inn);

        $this->assertEquals($expectedCompany, $actual);
    }

    const XML_UPDATE_WITH_ONE_MIDDLE_ENTREPRENEUR = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<Файл>
    <ИдОтпр>
        <ФИООтв Фамилия="_" Имя="_"/>
    </ИдОтпр>
    <Документ ИдДок="4562d850-52ed-4a1c-875f-c91b85844e0c" ВидСубМСП="2" КатСубМСП="3">
        <ИПВклМСП ИННФЛ="631805182801">
        </ИПВклМСП>
    </Документ>
</Файл>
XML;

    public function testUpdateParsing_entrepreneur()
    {
        $expectedEntrepreneur = SmallBusiness::aMiddle()->entrepreneur()->withInn('631805182801');
        Util::createZipArchiveWithFilesContents($this->zipUpdatePath, ['u.xml' => self::XML_UPDATE_WITH_ONE_MIDDLE_ENTREPRENEUR]);

        $this->parseSmallBusinessesZipUpdateCommand->run($this->zipUpdatePath);
        $actual = $this->smallBusinessStore->getOneByInn($expectedEntrepreneur->inn);

        $this->assertEquals($expectedEntrepreneur, $actual);
    }

    const UPDATE_PATH = '/home/alexey/data-05102019-structure-08012016.zip';

    public function testSavesUpdateMetadata()
    {
        $expectedUpdateMetadata = UpdateMetadata::createSmallBusinessesUpdateMetadataFromUpdateNameAndDataDate(
            'data-05102019-structure-08012016.zip',
            '2019-05-10'
        );
        $parseZipUpdateCommand = new ParseZipUpdateCommand(
            $this->createMock(ZipUpdateParser::class),
            $this->updateMetadataStore,
            new UpdatePathToUpdateMetadataConverter(new UpdateFilenameValidator('08012016'))
        );

        $parseZipUpdateCommand->run(self::UPDATE_PATH);
        $actual = $this->updateMetadataStore->get();

        $this->assertEquals($expectedUpdateMetadata, $actual);
    }
}