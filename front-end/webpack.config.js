const webpack = require('webpack');
const path = require('path');
const merge = require('webpack-merge');
const ManifestPlugin = require('webpack-manifest-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const WriteFilePlugin = require('write-file-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

REGEXP = {
    css: /\.css$/,
    images: /\.(png|jpg|gif|svg)$/,
    fonts: /\.(eot|ttf|woff|woff2)$/,
    js: /\.js$/    
};

const commonConfig = {
    entry: {
        app: path.join(__dirname, './src/app')
    },
    output: {
        path: path.join(__dirname, '../web/dist'),
        filename: '[name].[chunkhash].js'

    },
    resolve: {
        modules: ['node_modules', 'bower_components'],
        descriptionFiles: ['package.json', 'bower.json']
    },
    plugins: [
        new CleanWebpackPlugin(['dist'], {
            root: path.join(__dirname, '../web')
        }),
        new webpack.ProvidePlugin({
            '$': 'jquery',
            'jQuery': 'jquery',
            'window.jQuery': 'jquery',
            Popper: ['popper.js', 'default'],
            Tether: 'tether',
            'window.Tether': 'tether',
        }),
    ],
    module: {
        rules: [
            {
                test: /\.html$/,
                exclude: /node_modules/,
                use: { loader: 'html-loader' }
            }
        ]
    }
};

const productionConfig = {
    mode: 'production',
    plugins: [
        new ManifestPlugin({
            basePath: 'dist/',
            publicPath: 'dist/',
            writeToFileEmit: true
        }),
        new WriteFilePlugin(),
        new MiniCssExtractPlugin({
            filename: '[name].[chunkhash].css'
        })
    ],
    module: {
        rules: [
            {
                test: REGEXP.css,
                use: [
                    { loader: MiniCssExtractPlugin.loader },
                    'css-loader'
                ]
            },
            {
                test: REGEXP.images,
                loader: 'file-loader',
                options: {
                    name: "images/[hash].[ext]"
                }
            },
            {
                test: REGEXP.fonts,
                loader: 'file-loader',
                options: {
                    name: "fonts/[hash].[ext]"
                }
            },
            {
                test: REGEXP.js,
                exclude: /node_modules|bower_components/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    optimization: {
        splitChunks: {
            chunks: 'all'
        }
    }
};

const developmentConfig = {
    mode: 'development',
    devtool: 'source-map',
    output: {
        publicPath: 'http://localhost:8080/dist/',
        filename: '[name].js'
    },
    plugins: [
        new ManifestPlugin({
            basePath: 'dist/',
            publicPath: 'http://localhost:8080/dist/',
            writeToFileEmit: true
        }),
        new webpack.HotModuleReplacementPlugin()
    ],
    module: {
        rules: [
            {
                test: REGEXP.css,
                use: ['style-loader', 'css-loader']
            },
            {
                test: REGEXP.images,
                loader: 'url-loader'
            },
            {
                test: REGEXP.fonts,
                loader: 'url-loader'
            },
        ]
    },
    devServer: {
        hotOnly: true,
        inline: true,
        headers: {
            'Access-Control-Allow-Origin': '*'
        },
        contentBase: path.join(__dirname, 'web/'),
    }
};

module.exports = env => {
    if (env === 'prod') {
        return merge(commonConfig, productionConfig);
    } else if (env === 'dev') {
        return merge(commonConfig, developmentConfig);
    } else {
        throw new Error('Unknown environment: ' + env);
    }

};

