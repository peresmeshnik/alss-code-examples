import _ from 'lodash';
import $ from 'jquery';
import { startCaseString } from '../shared/helpers';
import exchangeRatesConverter from './exchange-rates-converter';

const EXCHANGE_RATES_URL = 'https://www.cbr-xml-daily.ru/daily_json.js';

class ExchangeRatesStore {
    constructor() {
        this.cachedRates = [];
        this.date;
	}
    
    /**
     * @callback getRatesByCodesCallback
     * @param {Array} rates 
     */

    /**
     * @param {string[]} codes
     * @param {getRatesByCodesCallback} callback
     * @return {Array}
     */
    getRatesByCodes(codes, callback) {
        if (!_.isEmpty(this.cachedRates)) {
            callback(this.cachedRates);
        }

        $.getJSON(EXCHANGE_RATES_URL, (rawRates) => {
            this._filterRatesByCodes(
                exchangeRatesConverter.convert(rawRates),
                codes,
                (filteredRates) => {
                    this.cachedRates = filteredRates;
                    callback(filteredRates);
                }
            );
        });
    }

    getDateFromApiRsponse() {
        $.getJSON(EXCHANGE_RATES_URL, (rawRates) => {
            this.date =  rawRates.Date;
        });
    }

    /**
     * @callback filterRatesByCodesCallback
     * @param {Array} filteredRates
     */

    /**
     * @param {Array} exchangeRates
     * @param {string[]} codes 
     * @param {filterRatesByCodesCallback} callback 
     */
    _filterRatesByCodes(exchangeRates, codes, callback) {
        let filteredRates = [];

        _.forEach(exchangeRates, (currency) => {
            if (codes.includes(currency.code)) {
                filteredRates.push(currency);
            }
        });

        callback(filteredRates);
    }
}

export default new ExchangeRatesStore();