import $ from 'jquery';
import './exchange-rates.css';
import ExchangeRates from './exchange-rates';
import exchangeRatesStore from './exchange-rates-store';

export default (elementId) => {
    $(document).ready(() => {
        let exchangeRates = new ExchangeRates(document.getElementById(elementId));

        exchangeRatesStore.getDateFromApiRsponse();

        exchangeRatesStore.getRatesByCodes(['USD', 'EUR', 'CNY', 'JPY'], (rates) => {
            exchangeRates.updateWith(rates);
        })

    })
};
