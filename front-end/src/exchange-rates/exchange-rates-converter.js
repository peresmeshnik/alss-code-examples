import _ from 'lodash';

class ExchangeRatesConverter {
    constructor() {
    }

    /**
     * @param {Object} apiResponse 
     */
    convert(apiResponse) {
        let result = [];

        _.forOwn(apiResponse.Valute, (apiCurrency, key) => {
            let currency = {};
            currency.code = apiCurrency.CharCode;
            currency.value = (apiCurrency.Value / apiCurrency.Nominal).toFixed(4).replace(".", ",");
            currency.trend = this._getCurrencyTrendFrom(apiCurrency);
            result.push(currency);
        });

        return result;
    }

    _getCurrencyTrendFrom(currency) {
        let result = 0;
        let current = currency.Value;
        let previous = currency.Previous;

        if (current > previous) {
            result = 1;
        } else if (current < previous) {
            result = -1;
        }
        
        return result;
    }
}

export default new ExchangeRatesConverter;