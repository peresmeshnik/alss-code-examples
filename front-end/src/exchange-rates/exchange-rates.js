import $ from 'jquery';
import _ from 'lodash';
import exchangeRatesStore from './exchange-rates-store';
import { parseDate } from '../shared/helpers';
import template from './exchange-rates.html';

class ExchangeRates {
    constructor(element) {
        this.element = element;
        this.template = _.template(template);
    }

    updateWith(rates) {
        const html = this.template({exchangeRates: this._normalizeRates(rates)});
        $(this.element).replaceWith(html);
    }

    /**
     * @param {Array} rates
     */
    _normalizeRates(rates) {
        let result = {
            "currencies": [],
            "date" : ""
        };

        _.forEach(rates, (currency) => {
            this._makeCurrencyCodeLowerCase(currency);
			this._makeCurrencyTrendSymbolic(currency);
			result['currencies'].push(currency);
		});

        result['date'] = parseDate(exchangeRatesStore.date, 'DATE'); 

        return result;
    }

    _makeCurrencyCodeLowerCase(currency) {
        currency.code = currency.code.toLowerCase();
    }

    _makeCurrencyTrendSymbolic(currency) {
        switch (currency.trend) {
            case 1:
                currency.trend = 'up';
                break;
            case -1:
                currency.trend = 'down';
                break;
            default:
                currency.trend = 'flat';
        }
    }
}

export default ExchangeRates